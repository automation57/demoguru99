from web_app.pages.page_main import PAGE_MAIN
from web_app.locators.locators_mobile import *


class PAGE_MOBILE(PAGE_MAIN):

    def verifyMobilePageTitle(self, expectedTitle):
        title = self.driver.find_element_by_xpath(mobilePageTitle).text
        if expectedTitle == title:
            print("Title '"+expectedTitle+"' "
                  "is shown on mobile list page. Verify PASS")
        else:
            print("Title '"+expectedTitle+"' "
                  "is not shown on mobile list page. Verify FAILED")

    def sortFunction(self, sortBy="Name", ascending=True):
        self.driver.find_element_by_xpath(mobileDropBoxSort).click()
        self.driver.find_element_by_xpath(mobileSortBy.format(sortBy)).click()
        if not ascending:
            self.driver.find_element_by_xpath(mobileSortDecrease).click()

    def getProductsInfo(self, product="all"):
        i = 1
        productsdict = {}
        productstotal = self.driver.find_elements_by_xpath(mobileProductNumber)

        if product == "all":
            for prod in productstotal:
                productname = self.driver.find_element_by_xpath(
                    mobileProductNameNumber.format(str(i))).text

                try:
                    productcost = self.driver.find_element_by_xpath(
                        mobileProductCost.format(str(i))).text
                except:
                    productcost = self.driver.find_element_by_xpath(
                        mobileProductSpecialCost.format(str(i))).text

                productsdict[productname] = {"cost": productcost}
                i += 1

        else:
            try:
                productcost = self.driver.find_element_by_xpath(
                    mobileProductCostByName.format(product)).text
            except:
                productcost = self.driver.find_element_by_xpath(
                    mobileProductSpecialCostByName.format(product)).text

            productsdict[product] = {"cost": productcost}

        return productsdict

    def getProductsName(self, productsDict):
        productslist = []
        for name in productsDict:
            productslist.append(name)

        listlength = len(productslist)
        return productslist, listlength

    def verifyProductsNumber(self, actualNumber, expectedNumber):
        if actualNumber == expectedNumber:
            print("Number of products is "+str(expectedNumber)+". "
                  "Verify PASS")
        else:
            print("Number of products is not "+str(expectedNumber)+". "
                  "Verify FAILED")

    def verifySort(self, productsList, ascending=True):
        if ascending:
            sortedproducts = sorted(productsList)
        else:
            sortedproducts = sorted(productsList, reverse=True)

        if sortedproducts == productsList:
            print("All products are sorted correctly. Verify PASS")
        else:
            print("All products are not sorted correctly. Verify FAILED")
