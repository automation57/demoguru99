from web_app.pages.page_main import PAGE_MAIN
from web_app.locators.locators_home import *


class PAGE_HOME(PAGE_MAIN):

    def selectMobilePage(self):
        self.driver.find_element_by_xpath(homeMobilePage).click()

    def verifyHomePageTitle(self, expectedTitle):
        title = self.driver.find_element_by_xpath(homePageTitle).text
        if expectedTitle in title:
            print("Text '"+expectedTitle+"' is shown in home page. Verify PASS")
        else:
            print("Text '"+expectedTitle+"' is not shown in home page. Verify FAILED")
