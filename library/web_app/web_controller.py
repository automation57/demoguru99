from selenium import webdriver


class WebController(object):

    def __init__(self, browser='chrome'):
        self.driver = None
        if browser.lower() == 'chrome':
            self.driver = webdriver.Chrome()
        elif browser.lower() == 'firefox':
            self.driver = webdriver.Firefox()
        self.driver.maximize_window()

    def openBrowser(self, url):
        self.driver.get(url)
