from web_app.pages.page_home import PAGE_HOME
from web_app.pages.page_mobile import PAGE_MOBILE
from web_app.pages.page_tv import PAGE_TV


class WEB_LIB(PAGE_HOME, PAGE_MOBILE, PAGE_TV):

    def __init__(self, browser='chrome'):
        super(WEB_LIB, self).__init__(browser)

    def __del__(self):
        self.driver.close()


if __name__ == '__main__':
    web_lib = WEB_LIB()
