

# Home page title
confHomePageTitle = "THIS IS DEMO SITE"

# Mobile page title
confMobilePageTitle = "MOBILE"

# Sort by info
confSortBy = "Name"

# Products number
confProductsNumber = 3

# Set ascending direction
confAscending = True
