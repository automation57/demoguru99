from library.web_app.web_lib import WEB_LIB
from suites.web_app.data.web_conf import *
from suites.common_data.common_data import *


if __name__ == '__main__':
    web_ctrl = WEB_LIB(common_browser)
    web_ctrl.openBrowser(common_url)
    # Verify step 1
    web_ctrl.verifyHomePageTitle(confHomePageTitle)
    web_ctrl.selectMobilePage()
    # Verify step 2
    web_ctrl.verifyMobilePageTitle(confMobilePageTitle)
    web_ctrl.sortFunction(confSortBy, confAscending)
    allProductsInfo = web_ctrl.getProductsInfo()
    productsNameList = web_ctrl.getProductsName(allProductsInfo)
    # Verify step 3
    web_ctrl.verifyProductsNumber(productsNameList[1], confProductsNumber)
    web_ctrl.verifySort(productsNameList[0], confAscending)
