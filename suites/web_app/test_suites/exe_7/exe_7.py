from library.web_app.web_lib import WEB_LIB
from suites.web_app.data.exe_7.exe_7_conf import *
from suites.common_data.common_data import *


if __name__ == '__main__':
    web_ctrl = WEB_LIB(common_browser)
    web_ctrl.openBrowser(common_url)
